<<<<<<< HEAD
drop database if exists tgnptsandfs;
create database tgnptsandfs;
use tgnptsandfs;
create table usuario(
	idUsuario int auto_increment,
    nombreUsuario varchar(200) NOT NULL,
    apellidoUsuario varchar(200),
    emailUsuario varchar(100) UNIQUE,
    telefonoUsuario int(9),
    fechaNacimiento date,
    imagenUsuario blob,
    nomUsuario varchar(40) UNIQUE,
    claveUsuario varchar(30),
    rolUsuario char(10) default 'usuario',
    check (rolUsuario='usuario' or rolUsuario='protectora' or rolUsuario='admin'),
    primary key(idUsuario)
) ENGINE=InnoDB;
INSERT INTO usuario (nombreUsuario,apellidoUsuario,emailUsuario,telefonoUsuario,fechaNacimiento,imagenUsuario,nomUsuario,claveUsuario,rolUsuario) VALUES ('Pablo','Peréz Maño','perpa@gmail.com',654321987,'1984-02-14',NULL,'perpa','asdf1234','usuario');
INSERT INTO usuario (nombreUsuario,apellidoUsuario,emailUsuario,telefonoUsuario,fechaNacimiento,imagenUsuario,nomUsuario,claveUsuario,rolUsuario) VALUES ('Beatriz','Sánchez López','bealopez@gmail.com',639258147,'1967-09-01',NULL,'bea','123','usuario');
INSERT INTO usuario (nombreUsuario,apellidoUsuario,emailUsuario,telefonoUsuario,fechaNacimiento,imagenUsuario,nomUsuario,claveUsuario,rolUsuario) VALUES ('Hernan','Gómez Fernández','herni34@hotmail.com',624852741,'1984-11-25',NULL,'hernan','hernan','usuario');
INSERT INTO usuario (nombreUsuario,apellidoUsuario,emailUsuario,telefonoUsuario,fechaNacimiento,imagenUsuario,nomUsuario,claveUsuario,rolUsuario) VALUES ('Protectora','Pato','pato_protectora@ter.es',632587419,NULL,NULL,'pato','pato','protectora');
INSERT INTO usuario (nombreUsuario,apellidoUsuario,emailUsuario,telefonoUsuario,fechaNacimiento,imagenUsuario,nomUsuario,claveUsuario,rolUsuario) VALUES ('Admin','','admin@paf.com',NULL,NULL,NULL,'admin','admin','admin');
INSERT INTO usuario (nombreUsuario,apellidoUsuario,emailUsuario,telefonoUsuario,fechaNacimiento,imagenUsuario,nomUsuario,claveUsuario,rolUsuario) VALUES ('Daniel','Mimbrero','dmimbrero@gmail.com',619693865,'1995-04-13',NULL,'dani','dmimbrero','usuario');
INSERT INTO usuario (nombreUsuario,apellidoUsuario,emailUsuario,telefonoUsuario,fechaNacimiento,imagenUsuario,nomUsuario,claveUsuario,rolUsuario) VALUES ('David','Fernández','dfernandez@gmail.com',658896325,'1993-05-15',NULL,'david','dfernandez','usuario');
INSERT INTO usuario (nombreUsuario,apellidoUsuario,emailUsuario,telefonoUsuario,fechaNacimiento,imagenUsuario,nomUsuario,claveUsuario,rolUsuario) VALUES ('Camila','Figueroa','cfigueroa@gmail.com',693852741,'1992-05-13',NULL,'camila','cfigueroa','usuario');
INSERT INTO usuario (nombreUsuario,apellidoUsuario,emailUsuario,telefonoUsuario,fechaNacimiento,imagenUsuario,nomUsuario,claveUsuario,rolUsuario) VALUES ('Felipe','Zapata','fzapata@gmail.com',663896896,'1996-08-01',NULL,'felipe','fzapata','usuario');
create table publicacion(
	idPublicacion int auto_increment,
    idUsuario int not null,
    fechaPublicacion date,
    tituloPublicacion varchar(100),
    descripcionPublicacion varchar(500),
    ubicacionPublicacion varchar(255),
    imagenPublicacion blob,
    primary key(idPublicacion),
    foreign key(idUsuario) references usuario(idUsuario) on delete cascade on update cascade
) ENGINE=InnoDB;
INSERT INTO publicacion (idUsuario,fechaPublicacion,tituloPublicacion,descripcionPublicacion,ubicacionPublicacion) VALUES (2,'2019-06-20','Cuidado','El Persa es una raza de gato conocido por tener una cara ancha, plana, y sumado a un abundante pelaje que suele ser de varios colores y tonalidades.

Como su nombre lo indica, esta raza tiene su origen en la antigua Persia (Irán en la actualidad) en el siglo XVII. 

Tienen un tamaño por lo general entre mediano y grande. La cabeza redonda y maciza. La parte de frente tiene una forma rodendeada, sumado a unos amplios pómulos.','Reus');
INSERT INTO publicacion (idUsuario,fechaPublicacion,tituloPublicacion,descripcionPublicacion,ubicacionPublicacion) VALUES (1,'2019-04-30','Abonadono','El perro de raza Yorkshire es el resultado de la combinaciÃ³n de terriers provenientes de Escocia e Inglaterra. Se caracteriza por su tamaño pequeño y pelaje brilloso y largo, razán por la cual siempre tuvo fama de un buen perro de exposición.

Su temparamento  tiende a ser activo y sobreprotector. Su adiestramiento suele ser fÃ¡cil en comparacipán otros perros. También posee fama de ladrar mucho, lo cual, los convierten en buenos perros vigilantes.','Reus');
INSERT INTO publicacion (idUsuario,fechaPublicacion,tituloPublicacion,descripcionPublicacion,ubicacionPublicacion) VALUES (3,'2018-12-10','Ofrecer','Platero es pequeño, peludo, suave; tan blando por fuera, que se diría todo de algodon, que no lleva huesos. Solo los espejos de azabache de sus ojos son duros cual dos escarabajos de cristal negro.

Lo dejo suelto, y se va al prado, y acaricia tibiamente con su hocico, rozándolas apenas, las florecillas rosas, celestes y gualdas... Lo llamo dulcemente: «¿Platero?»','Reus');
INSERT INTO publicacion (idUsuario,fechaPublicacion,tituloPublicacion,descripcionPublicacion,ubicacionPublicacion) VALUES (4,'2019-01-22','Ofrecer','Mi perro Sol, es pequeño como un cachorro, tiene el pelo suave como la seda y susojos son brillantes como dos luceros.Cuando me mira con sus luceros cuando estoy comiendo, ya se lo que quiere, micomida, que le parece tan deliciosa como las golosinas.','Reus');
INSERT INTO publicacion (idUsuario,fechaPublicacion,tituloPublicacion,descripcionPublicacion,ubicacionPublicacion) VALUES (3,'2019-05-04','Cuidado','Mi loro es hembra. Se llama Luna y es pequeña. sus alas son de color verde, la tripa amarilla y la garganta roja. Su pico también es rojo y bastante puntiagudo. Sus ojos son marrones y negros.Sus patas son cortas, pero los dedos son largos. En las patas tiene rayas negras y casi no se le ven. Cuando estoy en casa haciendo algo, está todo el ratogritando. Cuando nosotros comemos o cenamos, ella también empieza a comer.','Reus');

create table mascota(
	idMascota int auto_increment,
    idUsuario int not null,
    nombreMascota varchar(100),
    especieMascota varchar(100),
    razaMascota varchar(100),
    sexoMascota varchar(10),
    descripcionMascota varchar(200),
    imagenMascota blob,
    primary key(idMascota),
    foreign key(idUsuario) references usuario(idUsuario) on delete cascade on update cascade
) ENGINE=InnoDB;
INSERT INTO tgnptsandfs.mascota (idUsuario, nombreMascota, especieMascota, razaMascota, sexoMascota, descripcionMascota) VALUES (6, 'Zeus', 'Perro', 'Samoyero', 'Macho', 'Zeus es muy inquieto.');
INSERT INTO tgnptsandfs.mascota (idUsuario, nombreMascota, especieMascota, razaMascota, sexoMascota, descripcionMascota) VALUES (7, 'Miel', 'Perro', 'Mestizo', 'Hembra', 'A Miel le encantan los mimos.');
INSERT INTO tgnptsandfs.mascota (idUsuario, nombreMascota, especieMascota, razaMascota, sexoMascota, descripcionMascota) VALUES (8, 'Thor', 'Gato', 'Mestizo', 'Macho', 'Thor, el dios del trueno.');
INSERT INTO tgnptsandfs.mascota (idUsuario, nombreMascota, especieMascota, razaMascota, sexoMascota, descripcionMascota) VALUES (9, 'Oliver', 'Perro', 'Mestizo', 'Macho', 'Oliver, es mayorcito.');
INSERT INTO tgnptsandfs.mascota (idUsuario, nombreMascota, especieMascota, razaMascota, sexoMascota, descripcionMascota) VALUES (3, 'Lola', 'Perro', 'Chihuahua', 'Hembra', 'Loli, Muy cariñosa.');

create table comentario(
	idComentario int auto_increment,
    idPublicacion int not null,
    idUsuario int not null,
    comentario varchar(200),
    primary key(idComentario),
    foreign key(idPublicacion) references publicacion(idPublicacion) on delete cascade on update cascade,
    foreign key(idUsuario) references usuario(idUsuario) on delete cascade on update cascade
) ENGINE=InnoDB;
create table valoracion(
	idValoracion int auto_increment,
    idUsuario int not null,
    valoracion int(2) unsigned,
    comentarioValoracion varchar(200),
    primary key(idValoracion),
    foreign key(idUsuario) references usuario(idUsuario) on delete cascade on update cascade
) ENGINE=InnoDB;
CREATE TABLE seguidor (
  idSeguidor int(11) AUTO_INCREMENT,
  idUsuario int(11) NOT NULL,
  idSeguido int(11) NOT NULL,
  /* 	poner fecha actual			 */
  fechaSigue date DEFAULT NULL,
  fechaNoSigue date DEFAULT NULL,
  PRIMARY KEY (idSeguidor),
  CONSTRAINT seguidor_ibfk_1 FOREIGN KEY (idUsuario) REFERENCES usuario (idUsuario) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT seguidor_ibfk_2 FOREIGN KEY (idSeguido) REFERENCES usuario (idUsuario) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;
=======
drop database if exists tgnptsandfs;
create database tgnptsandfs;
use tgnptsandfs;
create table usuario(
	idUsuario int auto_increment,
    nombreUsuario varchar(200) NOT NULL,
    apellidoUsuario varchar(200),
    emailUsuario varchar(100) UNIQUE,
    telefonoUsuario int(9),
    fechaNacimiento date,
    imagenUsuario blob,
    nomUsuario varchar(40) UNIQUE,
    claveUsuario varchar(30),
    rolUsuario char(10) default 'usuario',
    check (rolUsuario='usuario' or rolUsuario='protectora' or rolUsuario='admin'),
    primary key(idUsuario)
) ENGINE=InnoDB;
INSERT INTO usuario (nombreUsuario,apellidoUsuario,emailUsuario,telefonoUsuario,fechaNacimiento,imagenUsuario,nomUsuario,claveUsuario,rolUsuario) VALUES ('Pablo','Perz Mao','perpa@gmail.com',654321987,'1984-02-14',NULL,'perpa','asdf1234','usuario');
INSERT INTO usuario (nombreUsuario,apellidoUsuario,emailUsuario,telefonoUsuario,fechaNacimiento,imagenUsuario,nomUsuario,claveUsuario,rolUsuario) VALUES ('Beatriz','Snchez Lpez','bealopez@gmail.com',639258147,'1967-09-01',NULL,'bea','123','usuario');
INSERT INTO usuario (nombreUsuario,apellidoUsuario,emailUsuario,telefonoUsuario,fechaNacimiento,imagenUsuario,nomUsuario,claveUsuario,rolUsuario) VALUES ('Hernan','Gmez Fernndez','herni34@hotmail.com',624852741,'1984-11-25',NULL,'hernan','hernan','usuario');
INSERT INTO usuario (nombreUsuario,apellidoUsuario,emailUsuario,telefonoUsuario,fechaNacimiento,imagenUsuario,nomUsuario,claveUsuario,rolUsuario) VALUES ('Protectora','Pato','pato_protectora@ter.es',632587419,NULL,NULL,'pato','pato','protectora');
INSERT INTO usuario (nombreUsuario,apellidoUsuario,emailUsuario,telefonoUsuario,fechaNacimiento,imagenUsuario,nomUsuario,claveUsuario,rolUsuario) VALUES ('Admin','','admin@paf.com',NULL,NULL,NULL,'admin','admin','admin');
INSERT INTO usuario (nombreUsuario,apellidoUsuario,emailUsuario,telefonoUsuario,fechaNacimiento,imagenUsuario,nomUsuario,claveUsuario,rolUsuario) VALUES ('Daniel','Mimbrero','dmimbrero@gmail.com',619693865,'1995-04-13',NULL,'dani','dmimbrero','usuario');
INSERT INTO usuario (nombreUsuario,apellidoUsuario,emailUsuario,telefonoUsuario,fechaNacimiento,imagenUsuario,nomUsuario,claveUsuario,rolUsuario) VALUES ('David','Fernndez','dfernandez@gmail.com',658896325,'1993-05-15',NULL,'david','dfernandez','usuario');
INSERT INTO usuario (nombreUsuario,apellidoUsuario,emailUsuario,telefonoUsuario,fechaNacimiento,imagenUsuario,nomUsuario,claveUsuario,rolUsuario) VALUES ('Camila','Figueroa','cfigueroa@gmail.com',693852741,'1992-05-13',NULL,'camila','cfigueroa','usuario');
INSERT INTO usuario (nombreUsuario,apellidoUsuario,emailUsuario,telefonoUsuario,fechaNacimiento,imagenUsuario,nomUsuario,claveUsuario,rolUsuario) VALUES ('Felipe','Zapata','fzapata@gmail.com',663896896,'1996-08-01',NULL,'felipe','fzapata','usuario');
create table publicacion(
	idPublicacion int auto_increment,
    idUsuario int not null,
    fechaPublicacion date,
    tituloPublicacion varchar(100),
    descripcionPublicacion varchar(500),
    ubicacionPublicacion varchar(255),
    imagenPublicacion blob,
    primary key(idPublicacion),
    foreign key(idUsuario) references usuario(idUsuario) on delete cascade on update cascade
) ENGINE=InnoDB;
INSERT INTO publicacion (idUsuario,fechaPublicacion,tituloPublicacion,descripcionPublicacion,ubicacionPublicacion) VALUES (2,'2019-06-20','Cuidado','El Persa es una raza de gato conocido por tener una cara ancha, plana, y sumado a un abundante pelaje que suele ser de varios colores y tonalidades.

Como su nombre lo indica, esta raza tiene su origen en la antigua Persia (Irn en la actualidad) en el siglo XVII. 

Tienen un tamao por lo general entre mediano y grande. La cabeza redonda y maciza. La parte de frente tiene una forma rodendeada, sumado a unos amplios pmulos.','Reus');
INSERT INTO publicacion (idUsuario,fechaPublicacion,tituloPublicacion,descripcionPublicacion,ubicacionPublicacion) VALUES (1,'2019-04-30','Abonadono','El perro de raza Yorkshire es el resultado de la combinaci�n de terriers provenientes de Escocia e Inglaterra. Se caracteriza por su tamao pequeo y pelaje brilloso y largo, razn por la cual siempre tuvo fama de un buen perro de exposicin.

Su temparamento  tiende a ser activo y sobreprotector. Su adiestramiento suele ser f�cil en comparacipn otros perros. Tambin posee fama de ladrar mucho, lo cual, los convierten en buenos perros vigilantes.','Reus');
INSERT INTO publicacion (idUsuario,fechaPublicacion,tituloPublicacion,descripcionPublicacion,ubicacionPublicacion) VALUES (3,'2018-12-10','Ofrecer','Platero es pequeo, peludo, suave; tan blando por fuera, que se dira todo de algodon, que no lleva huesos. Solo los espejos de azabache de sus ojos son duros cual dos escarabajos de cristal negro.

Lo dejo suelto, y se va al prado, y acaricia tibiamente con su hocico, rozndolas apenas, las florecillas rosas, celestes y gualdas... Lo llamo dulcemente: Platero?','Reus');
INSERT INTO publicacion (idUsuario,fechaPublicacion,tituloPublicacion,descripcionPublicacion,ubicacionPublicacion) VALUES (4,'2019-01-22','Ofrecer','Mi perro Sol, es pequeo como un cachorro, tiene el pelo suave como la seda y susojos son brillantes como dos luceros.Cuando me mira con sus luceros cuando estoy comiendo, ya se lo que quiere, micomida, que le parece tan deliciosa como las golosinas.','Reus');
INSERT INTO publicacion (idUsuario,fechaPublicacion,tituloPublicacion,descripcionPublicacion,ubicacionPublicacion) VALUES (3,'2019-05-04','Cuidado','Mi loro es hembra. Se llama Luna y es pequea. sus alas son de color verde, la tripa amarilla y la garganta roja. Su pico tambin es rojo y bastante puntiagudo. Sus ojos son marrones y negros.Sus patas son cortas, pero los dedos son largos. En las patas tiene rayas negras y casi no se le ven. Cuando estoy en casa haciendo algo, est todo el ratogritando. Cuando nosotros comemos o cenamos, ella tambin empieza a comer.','Reus');

create table mascota(
	idMascota int auto_increment,
    idUsuario int not null,
    nombreMascota varchar(100),
    especieMascota varchar(100),
    razaMascota varchar(100),
    sexoMascota varchar(10),
    descripcionMascota varchar(200),
    imagenMascota blob,
    primary key(idMascota),
    foreign key(idUsuario) references usuario(idUsuario) on delete cascade on update cascade
) ENGINE=InnoDB;
INSERT INTO tgnptsandfs.mascota (idUsuario, nombreMascota, especieMascota, razaMascota, sexoMascota, descripcionMascota) VALUES (6, 'Zeus', 'Perro', 'Samoyero', 'Macho', 'Zeus es muy inquieto.');
INSERT INTO tgnptsandfs.mascota (idUsuario, nombreMascota, especieMascota, razaMascota, sexoMascota, descripcionMascota) VALUES (7, 'Miel', 'Perro', 'Mestizo', 'Hembra', 'A Miel le encantan los mimos.');
INSERT INTO tgnptsandfs.mascota (idUsuario, nombreMascota, especieMascota, razaMascota, sexoMascota, descripcionMascota) VALUES (8, 'Thor', 'Gato', 'Mestizo', 'Macho', 'Thor, el dios del trueno.');
INSERT INTO tgnptsandfs.mascota (idUsuario, nombreMascota, especieMascota, razaMascota, sexoMascota, descripcionMascota) VALUES (9, 'Oliver', 'Perro', 'Mestizo', 'Macho', 'Oliver, es mayorcito.');
INSERT INTO tgnptsandfs.mascota (idUsuario, nombreMascota, especieMascota, razaMascota, sexoMascota, descripcionMascota) VALUES (3, 'Lola', 'Perro', 'Chihuahua', 'Hembra', 'Loli, Muy cariosa.');

create table comentario(
	idComentario int auto_increment,
    idPublicacion int not null,
    idUsuario int not null,
    comentario varchar(200),
    primary key(idComentario),
    foreign key(idPublicacion) references publicacion(idPublicacion) on delete cascade on update cascade,
    foreign key(idUsuario) references usuario(idUsuario) on delete cascade on update cascade
) ENGINE=InnoDB;
create table valoracion(
	idValoracion int auto_increment,
    idUsuario int not null,
    valoracion int(2) unsigned,
    comentarioValoracion varchar(200),
    primary key(idValoracion),
    foreign key(idUsuario) references usuario(idUsuario) on delete cascade on update cascade
) ENGINE=InnoDB;
CREATE TABLE seguidor (
  idSeguidor int(11) AUTO_INCREMENT,
  idUsuario int(11) NOT NULL,
  idSeguido int(11) NOT NULL,
  /* 	poner fecha actual			 */
  fechaSigue date DEFAULT NULL,
  fechaNoSigue date DEFAULT NULL,
  PRIMARY KEY (idSeguidor),
  CONSTRAINT seguidor_ibfk_1 FOREIGN KEY (idUsuario) REFERENCES usuario (idUsuario) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT seguidor_ibfk_2 FOREIGN KEY (idSeguido) REFERENCES usuario (idUsuario) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;
>>>>>>> branch 'master' of https://gitlab.com/david92f/tgn-pets-and-friends.git
