package utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.Transaction;

import modelos.Mascota;
import modelos.Publicacion;
import modelos.Usuario;

public class MascotaService {

	public Usuario getUserById(String nomUsuario) {

		Session session = new HibernateUtils().getSessionFactory();
		Transaction tx = null;
		Usuario user = null;
		try {
			tx = session.getTransaction();
			tx.begin();
			TypedQuery<Usuario> query = session.createQuery("from Usuario where nomUsuario='" + nomUsuario + "'",
					Usuario.class);
			user = query.getSingleResult();
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return user;
	}

	// mostrar todas las publicaciones de un usuario segun el id
	public Set<Mascota> getListMascotasById(int id) {
		Session session = new HibernateUtils().getSessionFactory();
		session.beginTransaction();
		Set<Mascota> p= (Set<Mascota>)session.get(Usuario.class, id).getMascotas();
		session.close();
		return p;
	}
	
	public List<Mascota> getListMascota() {

		List<Mascota> lista = new ArrayList<Mascota>();
		Session session = new HibernateUtils().getSessionFactory();
		Transaction tx = null;
		try {
			tx = session.getTransaction();
			tx.begin();
			lista = session.createQuery("from Mascota", Mascota.class).list();
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return lista;
	}
}
