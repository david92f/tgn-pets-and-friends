package utils;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.TypedQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

import modelos.Usuario;

public class LoginService {

	public boolean authenticateUser(String userId, String password) {

		Usuario user = getUserByUserId(userId);

		if (user != null && user.getNomUsuario().equals(userId) && user.getClaveUsuario().equals(password)) {
			return true;
		} else {
			return false;
		}
	}

	public Usuario getUserByUserId(String userId) {

		Session session = new HibernateUtils().getSessionFactory();
		Transaction tx = null;
		Usuario usuario = null;
		try {
			tx = session.getTransaction();
			tx.begin();
			TypedQuery<Usuario> query = session.createQuery("from Usuario where nomUsuario='" + userId + "'",
					Usuario.class);
			usuario = query.getSingleResult();
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return usuario;
	}

	public List<Usuario> getListOfUsers() {
		List<Usuario> lista = new ArrayList<Usuario>();
		Session session = new HibernateUtils().getSessionFactory();
		Transaction tx = null;
		try {
			tx = session.getTransaction();
			tx.begin();
			lista = session.createQuery("from Usuario", Usuario.class).list();
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return lista;
	}
}
