package utils;

import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.Transaction;

import modelos.Usuario;

public class EditarPerfilService {

	public Usuario getUserByUserId(String userId) {

		Session session = new HibernateUtils().getSessionFactory();
		Transaction tx = null;
		Usuario usuario = null;
		try {
			tx = session.getTransaction();
			tx.begin();
			TypedQuery<Usuario> query = session.createQuery("from Usuario where nomUsuario='" + userId + "'",
					Usuario.class);
			usuario = query.getSingleResult();
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return usuario;
	}


public boolean update(Usuario usuario) {

	Session session = new HibernateUtils().getSessionFactory();

	Transaction tx = null;
	try {
		tx = session.getTransaction();
		tx.begin();
		session.update(usuario);
		tx.commit();
	} catch (Exception e) {
		if (tx != null) {
			tx.rollback();
		}
		e.printStackTrace();
	} finally {
		session.close();
	}
	return true;
}
}
