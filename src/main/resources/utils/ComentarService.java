package utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.TypedQuery;
import javax.servlet.http.HttpSession;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.TransactionException;
import org.hibernate.query.Query;

import modelos.Comentario;
import modelos.Publicacion;
import modelos.Usuario;

public class ComentarService {

	public void guardar(Comentario comentario) {

		Session session = new HibernateUtils().getSessionFactory();

		Transaction tx = null;
		try {
			tx = session.getTransaction();
			tx.begin();
			session.save(comentario);
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	public Usuario getUserById(String nomUsuario) {

		Session session = new HibernateUtils().getSessionFactory();
		Transaction tx = null;
		Usuario user = null;
		try {
			tx = session.getTransaction();
			tx.begin();
			TypedQuery<Usuario> query = session.createQuery("from Usuario where nomUsuario='" + nomUsuario + "'",
					Usuario.class);
			user = query.getSingleResult();
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return user;
	}

	// mostrar todos comentarios de un usuario segun el id
	public Set<Comentario> getListComentarioById(int id) {
		Session session = new HibernateUtils().getSessionFactory();
		session.beginTransaction();
		Set<Comentario> p = (Set<Comentario>) session.get(Usuario.class, id).getComentarios();
		session.close();
		return p;
	}

	public List<Comentario> getListComentario() {

		List<Comentario> lista = new ArrayList<Comentario>();
		Session session = new HibernateUtils().getSessionFactory();
		Transaction tx = null;
		try {
			tx = session.getTransaction();
			tx.begin();
			lista = session.createQuery("from Comentario").list();
			tx.commit();
		} catch (TransactionException e) {
			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return lista;
	}
}
