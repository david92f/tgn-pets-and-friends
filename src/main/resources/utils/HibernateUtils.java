package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.hibernate.Session;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import modelos.Usuario;

public class HibernateUtils {

	// Crea el session factory correctamente desde el hibernate.cfg.xml
	public Session getSessionFactory() {
		try {
			return new Configuration().configure().buildSessionFactory().openSession();
		} catch (HibernateException he) {
			System.out.println("La creacion del Session Factory fallo.\n" + he);
			throw he;
		}
	}
	
	// guarda en la base de datos la imagen que elijas ++++++++++++++ acabar ++++++++++++++++++ pasar por parametro el usuario y la ruta
	public void guardarImagen(Session session) {
		
		// guardar la imagen que hay en la siguiente ruta, la cual selecionaras  +++++++++
    	File file = new File("C:\\Users\\david\\git\\tgn-pets-and-friends\\src\\main\\webapp\\img\\logo.png");
        byte[] bFile = new byte[(int) file.length()];
        
        try {
	     FileInputStream fileInputStream = new FileInputStream(file);
	     fileInputStream.read(bFile);
	     fileInputStream.close();
        } catch (Exception e) {
	     e.printStackTrace();
        }
        // se tiene que pasar el usuario que quieras +++++++++++++++++++++
        Usuario u=new Usuario();
        u.setImagenUsuario(bFile);
        // guardas el usuario en la base de datos
        session.saveOrUpdate(u);
        session.getTransaction().commit();
	}
	
	// devuelve la imagen que elijas de la base +++++++++++++++++++++++++++ acabar ++++++++++++++++++ decir id usuario
	public byte[] mostrarImagen(Session session) {
		
		// descargar la imagen desde la base de datos
        Usuario u = (Usuario)session.get(Usuario.class, 3);
        byte[] bUsuario = u.getImagenUsuario();

        session.getTransaction().commit();
        return bUsuario;
	}
	
	// descargas la imagen que elijas de la base +++++++++++++++++++++++++++ acabar ++++++++++++++++++ decir id usuario
	public void descargarImagen(Session session) {
		// descargar la imagen desde la base de datos
        Usuario u = (Usuario)session.get(Usuario.class, 3);
        byte[] bUsuario = u.getImagenUsuario();
        
        try{
            FileOutputStream fos = new FileOutputStream("C:\\Users\\david\\git\\tgn-pets-and-friends\\src\\main\\webapp\\img\\test.jpg"); 
            fos.write(bUsuario);
            fos.close();
        }catch(Exception e){
            e.printStackTrace();
        }
	}

	
}