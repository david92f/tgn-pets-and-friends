package utils;

import javax.persistence.TypedQuery;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.TransactionException;

import modelos.Usuario;

public class RegisterService {

	public boolean register(Usuario usuario) {

		Session session = new HibernateUtils().getSessionFactory();
		if (isUserExists(usuario))
			return false;

		Transaction tx = null;
		try {
			tx = session.getTransaction();
			tx.begin();
			session.save(usuario);
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return true;
	}

	public boolean isUserExists(Usuario user) {

		Session session = new HibernateUtils().getSessionFactory();
		boolean result = false;
		Transaction tx = null;
		try {
			tx = session.getTransaction();
			tx.begin();
			TypedQuery<Usuario> query = session
					.createQuery("from Usuario where nomUsuario='" + user.getNombreUsuario() + "'", Usuario.class);
			Usuario u = (Usuario) query.getSingleResult();
			tx.commit();
			if (u != null)
				result = true;
		} catch (Exception ex) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return result;
	}
}