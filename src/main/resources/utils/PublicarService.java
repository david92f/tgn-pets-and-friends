package utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.TypedQuery;
import javax.servlet.http.HttpSession;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.TransactionException;
import org.hibernate.query.Query;

import modelos.Comentario;
import modelos.Publicacion;
import modelos.Usuario;

public class PublicarService {

	public void guardar(Publicacion publicacion) {

		Session session = new HibernateUtils().getSessionFactory();

		Transaction tx = null;
		try {
			tx = session.getTransaction();
			tx.begin();
			session.save(publicacion);
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	public Publicacion getpublicacionById(String id) {

		Session session = new HibernateUtils().getSessionFactory();
		Transaction tx = null;
		Publicacion post = null;
		try {
			tx = session.getTransaction();
			tx.begin();
			TypedQuery<Publicacion> query = session.createQuery("from Publicacion where idUsuario='" + id + "'",
					Publicacion.class);
			post = query.getSingleResult();
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return post;
	}

	// mostrar todas las publicaciones de un usuario segun el id
	public Set<Publicacion> getListPublicacionById(int id) {
		Session session = new HibernateUtils().getSessionFactory();
		session.beginTransaction();
		Set<Publicacion> p = (Set<Publicacion>) session.get(Usuario.class, id).getPublicacions();
		session.close();
		return p;
	}

	public List<Publicacion> getListPublicacion() {

		List<Publicacion> lista = new ArrayList<Publicacion>();
		Session session = new HibernateUtils().getSessionFactory();
		Transaction tx = null;
		try {
			tx = session.getTransaction();
			tx.begin();
			lista = session.createQuery("from Publicacion").list();
			tx.commit();
		} catch (TransactionException e) {
			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return lista;
	}
}
