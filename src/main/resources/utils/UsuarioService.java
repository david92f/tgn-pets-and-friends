package utils;

import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.Transaction;

import modelos.Usuario;

public class UsuarioService {
	public Usuario getUserById(String nomUsuario) {

		Session session = new HibernateUtils().getSessionFactory();
		Transaction tx = null;
		Usuario user = null;
		try {
			tx = session.getTransaction();
			tx.begin();
			TypedQuery<Usuario> query = session.createQuery("from Usuario where nomUsuario='" + nomUsuario + "'",
					Usuario.class);
			user = query.getSingleResult();
			tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return user;
	}
}
