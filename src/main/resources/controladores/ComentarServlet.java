package controladores;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modelos.Comentario;
import modelos.Publicacion;
import modelos.Usuario;
import utils.ComentarService;
import utils.PublicarService;

/**
 * Servlet implementation class ComentarServlet
 */
public class ComentarServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		Comentario c = new Comentario();
		// a�adir usuario desde httpsession
		Usuario u = (Usuario) request.getSession().getAttribute("usuario");

		c.setComentario(request.getParameter("descripcion"));
		c.setUsuario(u);
		String id=request.getParameter("idPost");
		c.setPublicacion(new PublicarService().getpublicacionById(id));

		// llamas a services y guarda la publicacion
		new ComentarService().guardar(c);

		// actualizar la pagina del muro para visualizar los cambios
		if (u.getRolUsuario().equals("usuario")) {
			response.sendRedirect("muro.jsp");
		} else {
			response.sendRedirect("muroAdmin.jsp");
		}
	}

}
