package controladores;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modelos.Usuario;
import utils.RegisterService;

public class RegisterServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = -193352208789686471L;

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("text/html;charset=UTF-8");

		Usuario usuario = new Usuario();
		usuario.setRolUsuario(request.getParameter("rol"));
		usuario.setNombreUsuario(request.getParameter("nomUsuario"));
		usuario.setApellidoUsuario(request.getParameter("apelUsuario"));
		usuario.setEmailUsuario(request.getParameter("emailUsuario"));
		String telefono = request.getParameter("telefonoUsuario");
		usuario.setTelefonoUsuario(new Integer(telefono));
		// metodo que formatea la fecha de nacimiento
		Date fechaNac = formateaFechaN(request.getParameter("fechaNac"));
		usuario.setFechaNacimiento(fechaNac);
		usuario.setNomUsuario(request.getParameter("alias"));
		// metodo que compara contraseņas 1 y 2
		String contra = validarContraseņa(request.getParameter("contra1"), request.getParameter("contra2"));
		usuario.setClaveUsuario(contra);

		try {
			RegisterService registerService = new RegisterService();
			boolean estaRegistrado = registerService.register(usuario);

			if (estaRegistrado) {
				response.sendRedirect("pag/registroCorrecto.jsp");
			} else {
				response.sendRedirect("pag/registroFallido.jsp");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// metodo que compara contraseņas 1 y 2
	private String validarContraseņa(String contra1, String contra2) {
		String contra = null;
		if (contra1.equals(contra2) == true)
			contra = contra1;
		return contra;
	}

	// metodo que formatea la fecha de nacimiento
	private Date formateaFechaN(String fecha) {
		String pattern = "yyyy-MM-dd";
		Date fechaN = null;

		try {
			DateFormat df = new SimpleDateFormat(pattern);
			fechaN = df.parse(fecha);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		// Using Java 8 Date and Time
		/*
		 * DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern); LocalDate
		 * fechaN = LocalDate.parse(fecha, formatter);
		 */

		return fechaN;
	}
}