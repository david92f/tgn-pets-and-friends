package controladores;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;

import modelos.Publicacion;
import modelos.Usuario;
import utils.HibernateUtils;
import utils.PublicarService;
import utils.RegisterService;

/**
 * Servlet implementation class PublicarServlet
 */
public class PublicarServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Publicacion p = new Publicacion();
		// a�adir usuario desde httpsession
		Usuario u = (Usuario) request.getSession().getAttribute("usuario");

		p.setTituloPublicacion(request.getParameter("titulo"));
		p.setUsuario(u);
		// publicar fecha actual
		Calendar cal = Calendar.getInstance();
		// sumar un mes mas (0-11)
		cal.add(Calendar.DATE, 1);
		Date date = cal.getTime();
		p.setFechaPublicacion(date);

		p.setDescripcionPublicacion(request.getParameter("descripcion"));
		String link = request.getParameter("imagen");

		// comprobar que no esta vacia la imagen
		if (!link.isEmpty()) {
			p.guardarImagen(link);
		}

		// llamas a services y guarda la publicacion
		new PublicarService().guardar(p);

		// actualizar la pagina del muro para visualizar los cambios
		if (u.getRolUsuario().equals("usuario")) {
			response.sendRedirect("muro.jsp");
		} else {
			response.sendRedirect("muroAdmin.jsp");
		}

	}

}
