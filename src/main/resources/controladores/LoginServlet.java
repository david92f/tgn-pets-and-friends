package controladores;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modelos.Publicacion;
import modelos.Usuario;
import utils.LoginService;
import utils.PublicarService;

public class LoginServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2844715505962689080L;

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String userId = request.getParameter("userId");
		String password = request.getParameter("password");
		LoginService loginService = new LoginService();
		// autentifica que existe el nombre y la clave
		boolean existe = loginService.authenticateUser(userId, password);
		// guarda el usuario segun el nombre de usuario
		Usuario usuario = loginService.getUserByUserId(userId);
		if (existe == true) {
			// guarda en httpsession a traves del request el usuario
			if(usuario.getRolUsuario().equals("admin")) {
				request.getSession().setAttribute("usuario", usuario);
				response.sendRedirect("muroAdmin.jsp");
			}else {
			request.getSession().setAttribute("usuario", usuario);
			response.sendRedirect("muro.jsp");
			}
		} else {
			response.sendRedirect("pag/errorLogin.jsp");
		}
	}

}