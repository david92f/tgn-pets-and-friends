package controladores;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mysql.cj.Session;

import modelos.Usuario;
import utils.EditarPerfilService;

/**
 * Servlet implementation class EditarPerfilServlet
 */
public class EditarPerfilServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
 protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	 String userId = request.getParameter("nomUsuario");
	 EditarPerfilService perfilService = new EditarPerfilService();
	 Usuario usuario = perfilService.getUserByUserId(userId);
	 
	 usuario.setNombreUsuario(request.getParameter("nombre"));
	 //String ape = request.getParameter("apellidos");
	 
	 try {
		EditarPerfilService updateServelt = new EditarPerfilService();
		boolean updateCorrecto = updateServelt.update(usuario);
		if (updateCorrecto == true) {
			 response.sendRedirect("perfilPropio.jsp");
		}
		
	 }
	 catch (Exception e) {
			e.printStackTrace();
		}
	 
	 
	 
	 /*if(!ape.isEmpty()) {
		 usuario.setApellidoUsuario(ape);
	 }*/
}

}
