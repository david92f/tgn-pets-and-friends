<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="es" class="no-js">

<head>
<link rel="shortcut icon" href="img/icono.ico">
<!-- Mobile Specific Meta -->
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Favicon-->
<link rel="shortcut icon" href="img/icono.ico">
<!-- Author Meta -->
<meta name="author" content="Colorlib">
<!-- Meta Description -->
<meta name="description" content="">
<!-- Meta Keyword -->
<meta name="keywords" content="">
<!-- meta character set -->
<meta charset="UTF-8">
<!-- Site Title -->
<title>Lista Usuarios</title>

<link
	href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700"
	rel="stylesheet">
<!--
			CSS
			============================================= -->
<link rel="stylesheet" href="css/linearicons.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/magnific-popup.css">
<link rel="stylesheet" href="css/nice-select.css">
<link rel="stylesheet"
	href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/ListaUsuarioEstilo.css">
<link rel="stylesheet" href="css/main.css">
<link rel="stylesheet" href="css/muro.css">

</head>

<body>

	<!-- Start Header Area -->
	<header class="default-header">
		<div class="container">
			<div class="header-wrap">
				<div
					class="header-top d-flex justify-content-between align-items-center">
					<div class="logo">
						<a href="muroAdmin.jsp#home"><img src="img/logo.png" alt=""></a>
					</div>
					<div class="main-menubar d-flex align-items-center">
						<nav class="hide">
							<a href="muroAdmin.jsp">Inicio</a> 
							<a href="logout.jsp"
								class="btn btn-outline-secondary">Cerrar sesi�n</a>
						</nav>
						<div class="menu-bar">
							<span class="lnr lnr-menu"></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>

	<!-- Listado de Usuarios -->
	<div class="container-fluid">
		<div class="row">
			<div class="col-12"></div>
		</div>
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<div class="card">
					<div class="card-body">
						<div class="row">
							<div class="col border-right">

								<h4 class="float-right">Usuario Admin</h4>

							</div>
							<div class="col">
								<button type="button" id="btnA�adir"
									class="btn btn-sm btn-primary">A�adir Usuario</button>
							</div>
							<div class="col"></div>


						</div>
						<div class="row">
							<div class="col-md-12">
								<table class="table table-hover table-responsive ">
									<thead class="bg-light ">
										<tr>
											<th>
												<div class="form-check-inline"></div>
											</th>
											<th colspan="4" scope="col">Lista usuario</th>

										</tr>
									</thead>
									<tbody>
										<tr>
											<td>
												<div class="form-check-inline"></div>
											</td>
											<td scope="row"><a href="#">Usuario1</a></td>
											<td><button type="button" class="btn btn-primary">MD</button></td>
											<td><button type="button" class="btn btn-primary">Eliminar</button></td>
											<td><button type="button" class="btn btn-primary">Bloquear</button></td>

										</tr>
										<tr>
											<td>
												<div class="form-check-inline"></div>
											</td>
											<td scope="row"><a href="#">Usuario2</a></td>
											<td><button type="button" class="btn btn-primary">MD</button></td>
											<td><button type="button" class="btn btn-primary">Eliminar</button></td>
											<td><button type="button" class="btn btn-primary">Bloquear</button></td>

										</tr>
										<tr>
											<td>
												<div class="form-check-inline"></div>
											</td>
											<td scope="row"><a href="#">Usuario3</a></td>
											<td><button type="button" class="btn btn-primary">MD</button></td>
											<td><button type="button" class="btn btn-primary">Eliminar</button></td>
											<td><button type="button" class="btn btn-primary">Bloquear</button></td>

										</tr>


									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
	<!-- start footer Area -->
	<footer class="footer-area section-gap">
		<div class="container">
			<div class="row d-flex flex-column justify-content-center">
				<ul class="footer-menu">
					<li><a href="listaUsuarios.jsp">Inicio</a></li>
					<li><a href="perfilPropio.jsp">Perfil</a></li>
					<li><a href="pag/logout.jsp">Cerrar Sesi�n</a></li>
				</ul>
				<div class="footer-social">
					<a href="#"> <i class="fa fa-facebook"></i>
					</a> <a href="#"> <i class="fa fa-twitter"></i>
					</a> <a href="#"> <i class="fa fa-instagram"></i>
					</a>
				</div>
				<p class="footer-text m-0">
					<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
					Copyright &copy;
					<script>
						document.write(new Date().getFullYear());
					</script>
					| The team Pets&Friends TGN <i class="fa fa-heart-o"
						aria-hidden="true"></i>
					<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
				</p>
			</div>
		</div>
	</footer>
	<!-- End footer Area -->

	<script src="../js/vendor/jquery-2.2.4.min.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"
		integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
		crossorigin="anonymous"></script>
	<script src="js/vendor/bootstrap.min.js"></script>
	<script src="js/jquery.ajaxchimp.min.js"></script>
	<script src="js/jquery.nice-select.min.js"></script>
	<script src="js/jquery.sticky.js"></script>
	<script src="js/parallax.min.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/main.js"></script>
</body>

</html>