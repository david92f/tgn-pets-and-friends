<%@page import="modelos.Usuario"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es" class="no-js">

<head>
<!-- Mobile Specific Meta -->
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Favicon-->
<link rel="shortcut icon" href="img/icono.ico">
<!-- Author Meta -->
<meta name="author" content="Colorlib">
<!-- Meta Description -->
<meta name="description" content="">
<!-- Meta Keyword -->
<meta name="keywords" content="">
<!-- meta character set -->
<meta charset="UTF-8">
<!-- Site Title -->
<title>Mensaje</title>


<link
	href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700"
	rel="stylesheet">


<link rel="stylesheet" href="css/linearicons.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/magnific-popup.css">
<link rel="stylesheet" href="css/nice-select.css">
<link rel="stylesheet"
	href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/main.css">
<link rel="stylesheet" href="css/muro.css">

<link
	href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
	rel="stylesheet"
	integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN"
	crossorigin="anonymous">

</head>


<body>

	<!-- Cargar los datos de la httpsession del usuario logeado -->
	<%
		Usuario u = (Usuario) session.getAttribute("usuario");
	
	%>

	<!-- BARRA BUSQUEDA -->

	<header class="default-header">
		<div class="container">
			<div class="header-wrap">
				<div
					class="header-top d-flex justify-content-between align-items-center">
					<div class="logo">
						<a href="muro.jsp#home"><img src="img/logo.png" alt=""></a>
					</div>

					<div class="main-menubar d-flex align-items-center">
						<nav class="hide">
							<a href="muro.jsp">Inicio</a> <a href="pag/logout.jsp"
								class="btn btn-outline-secondary">Cerrar sesión</a>
						</nav>
						<div class="media">

							<div class="menu-bar">
								<span class="lnr lnr-menu"></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>

	<!-- CONTENIDO NUEVO -->


	<div class="container-fluid gedf-wrapper muro">
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6 gedf-main">

				<div class="panel-heading clearfix">
					<center>
						<h3 class="panel-title">Enviar mensaje</h3>
					</center>
				</div>

				<div class="panel-body">

					<form method="post" action="MensajeServlet" class="form-horizontal">
						<div class="media">
							<a class="pull-left" href="#"> <img class="media-object dp "
								src="<%=u.urlImagen() %>" style="width: 100px; height: 100px;">
								<p><%=u.getNombreUsuario() %> <%=u.getApellidoUsuario() %></p>

							</a>
						</div>

						<div class="form-group">
							<div class="col-sm-30">
								<input type="text" class="form-control" id="inputSubject"
									placeholder="Asunto">
							</div>
						</div>

						<div class="form-group">
							<label class="sr-only" for="message">post</label>
							<textarea class="form-control" id="message" rows="3"
								placeholder="Escribir..."></textarea>
						</div>
						<center>
							<a href="pag/enviado.jsp" class="btn btn-success">¡Enviar!</a>
						</center>

					</form>

				</div>
			</div>
		</div>
	</div>

	<!-- CONTENIDO NUEVO -->

	<!-- start footer Area -->
	<!-- start footer Area -->
	<footer class="footer-area section-gap">
		<div class="container">
			<div class="row d-flex flex-column justify-content-center">
				<ul class="footer-menu">
					<li><a href="muro.jsp#home">Inicio</a></li>
					<li><a href="pag/logout.jsp" class="btn btn-outline-secondary">Cerrar
							sesión </a></li>
				</ul>
				<div class="footer-social">
					<a href="#"><i class="fa fa-facebook"></i></a> <a href="#"><i
						class="fa fa-twitter"></i></a> <a href="#"><i
						class="fa fa-instagram"></i></a>

				</div>
				<p class="footer-text m-0">
					<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
					Copyright &copy;
					<script>
						document.write(new Date().getFullYear());
					</script>
					| The team Pets&amp;Friends <i class="fa fa-heart-o"
						aria-hidden="true"></i>
					<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
				</p>
			</div>
		</div>
	</footer>




	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
		integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
		integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
		crossorigin="anonymous"></script>


	<script src="js/vendor/jquery-2.2.4.min.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"
		integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
		crossorigin="anonymous"></script>
	<script src="js/vendor/bootstrap.min.js"></script>
	<script src="js/jquery.ajaxchimp.min.js"></script>
	<script src="js/jquery.nice-select.min.js"></script>
	<script src="js/jquery.sticky.js"></script>
	<script src="js/parallax.min.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/main.js"></script>

</body>
</html>