<%@page import="modelos.Mascota"%>
<%@page import="java.util.Set"%>
<%@page import="modelos.Publicacion"%>
<%@page import="utils.PublicarService"%>
<%@page import="modelos.Usuario"%>
<%@ page language="java" contentType="text/html; charset=utf8"
	pageEncoding="utf8"%>
<!DOCTYPE html>
<html lang="es" class="no-js">

<head>
<!-- Mobile Specific Meta -->
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Favicon-->
<link rel="shortcut icon" href="img/icono.ico">
<!-- Author Meta -->
<meta name="author" content="Colorlib">
<!-- Meta Description -->
<meta name="description" content="">
<!-- Meta Keyword -->
<meta name="keywords" content="">
<!-- meta character set -->
<meta charset="UTF-8">
<!-- Site Title -->
<title>Perfil - TGN Pets &amp; Friends</title>

<link
	href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700"
	rel="stylesheet">


<link rel="stylesheet" href="css/linearicons.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/magnific-popup.css">
<link rel="stylesheet" href="css/nice-select.css">
<link rel="stylesheet"
	href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/main.css">
<link rel="stylesheet" href="css/muro.css">

<link
	href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
	rel="stylesheet"
	integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/nuestrosEstilos.css">
</head>

<body>

	<!-- Cargar los datos de la httpsession del usuario logeado -->
	<%
		Usuario u = (Usuario) session.getAttribute("usuario");
	
	%>

	<!-- Start Header Area -->
	<header class="default-header">
		<div class="container">
			<div class="header-wrap">
				<div
					class="header-top d-flex justify-content-between align-items-center">
					<div class="logo">
						<%
						if(u.getRolUsuario().equals("admin")){
						%>
						<a href="muroAdmin.jsp#home"><img src="img/logo.png" alt=""></a>
						<%
						}
						if(u.getRolUsuario().equals("usuario")){
						%>
						<a href="muro.jsp#home"><img src="img/logo.png" alt=""></a>

						<%
						} 
						%>
					</div>
					<div class="main-menubar d-flex align-items-center">
						<nav class="hide">
						<%
						if(u.getRolUsuario().equals("admin")){
						%>
							<a href=muroAdmin.jsp#home>Inicio</a> 
							
						<%
						}
						if(u.getRolUsuario().equals("usuario")){
						%>
						<a href=muro.jsp#home>Inicio</a>
						
						<%
						} 
						%>
						<a href="perfilPropio.jsp?user=<%=u.getNomUsuario()%>">Perfil</a>
							<a href="pag/logout.jsp"
								class="btn btn-outline-secondary">Cerrar sesión </a>
						
						</nav>
						<div class="menu-bar">
							<span class="lnr lnr-menu"></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>

	<!-- Perfil propio -->
	<%
	if(u.getNomUsuario().equals(request.getParameter("user"))){
	%>
	<div class="container-fluid miPerfil">
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-3">
				<div class="card">
					<div class="card-body">
						<div class="imgPerfil">
							<img class="img-thumbnail" src="<%=u.urlImagen() %>" alt="">
						</div>
						<div class="h5"><%=u.getNombreUsuario()%></div>
						<div class="h7 text-muted"><%=u.getApellidoUsuario()%></div>
					</div>
					<a href="mascotas.jsp" class="list-group-item">Mascotas</a>
					<ul class="list-group list-group-flush"> 
					
						<%Set<Mascota> listamascotas = u.getMascotas();
						for(Mascota m : listamascotas){
						%>
						<li class="list-group-item">
							<div class="h6"><%=m.getNombreMascota() %></div>
							<div class="h5 text-muted"><%=m.getEspecieMascota()%></div>
							
						</li>
						<%
						}
						%>
						<li class="list-group-item"><a href="valoraciones.jsp?user=<%=request.getParameter("user") %>">Valoraciones</a></li>
						<li class="list-group-item"><a href="editarPerfil.jsp">Edita
								tu perfil</a></li>
					</ul>
				</div>
			</div>
			<div class="col-md-7 gedf-main">
				<div class="card gedf-card">
					<div class="card-header">
						<ul class="nav nav-tabs card-header-tabs" id="myTab"
							role="tablist">
							<li class="nav-item"><a class="nav-link active"
								id="posts-tab" data-toggle="tab" href="#posts" role="tab"
								aria-controls="posts" aria-selected="true">Crear publicación</a>
							</li>
							<li class="nav-item"><a class="nav-link" id="images-tab"
								data-toggle="tab" role="tab" aria-controls="images"
								aria-selected="false" >Subir imágen</a></li>
						</ul>
					</div>
					<div class="card-body">
						<div class="tab-content" id="myTabContent">
							<div class="tab-pane fade show active" id="posts" role="tabpanel"
								aria-labelledby="posts-tab">
								<div class="form-group">

											<select name="type" class="app-select form-control" required>
												<option data-display="Qué quiere contar?"></option>
												<option value="1">Cuidado</option>
												<option value="2">Adopción</option>
												<option value="3">Ofrecer</option>
							 				</select>

									<label class="sr-only" for="message">post</label>
									<textarea class="form-control" id="message" rows="3"
										placeholder="Escribe algo..."></textarea>
								</div>

							</div>
							<div class="tab-pane fade" id="images" role="tabpanel"
								aria-labelledby="images-tab">
								<div class="form-group">
									<div class="custom-file">
										<input type="file" class="custom-file-input" id="customFile">
										<label class="custom-file-label" for="customFile">Subir</label>
									</div>
								</div>
								<div class="py-4"></div>
							</div>
						</div>
						<div class="btn-toolbar justify-content-between">
							<div class="btn-group">
								<button type="submit" class="btn btn-primary">Comparte</button>
							</div>
							<div class="btn-group"></div>
						</div>
					</div>
				</div>
				<!-- Post /////-->

				<!--- \\\\\\\Post-->
				<%
					PublicarService ps = new PublicarService();
					Set<Publicacion> lista = ps.getListPublicacionById(u.getIdUsuario());
					for (Publicacion p : lista) {
				%>
				<div class="card gedf-card">
					<div class="card-header">
						<div class="d-flex justify-content-between align-items-center">
							<div class="d-flex justify-content-between align-items-center">
								<div class="mr-2">
									<img class="rounded-circle" width="45" src="<%=p.getUsuario().urlImagen() %>" alt="">
								</div>
								<div class="ml-2">
									<div class="h5 m-0"><%=p.getUsuario().getNomUsuario()%></div>
									<div class="h7 text-muted">
										<p><%=p.getUsuario().getNombreUsuario()%> <%=p.getUsuario().getApellidoUsuario() %></p>
									</div>
								</div>
							</div>
							<div>
								<div class="dropdown">
									<button class="btn btn-link dropdown-toggle" type="button"
										id="gedf-drop1" data-toggle="dropdown" aria-haspopup="true"
										aria-expanded="false">
										<i class="fa fa-ellipsis-h"></i>
									</button>
									<div class="dropdown-menu dropdown-menu-right"
										aria-labelledby="gedf-drop1">
										<div class="h6 dropdown-header">Configuration</div>
										<a class="dropdown-item" href="#">Save</a> <a
											class="dropdown-item" href="#">Hide</a> <a
											class="dropdown-item" href="#">Report</a>
									</div>
								</div>
							</div>
						</div>

					</div>
					<div class="card-body">
						<div class="content-image img-fluid d-block mx-auto">
							<img src="<%=p.urlImagen() %>">
						</div>
						<div class="text-muted h7 mb-2">
							<i class="fa fa-clock-o "></i>
							<%=p.getFechaPublicacion()%>
						</div>
						<a class="card-link" href="#">
							<h5 class="card-title"><%=p.getTituloPublicacion()%></h5>
						</a>

						<p class="card-text"><%=p.getDescripcionPublicacion()%></p>
					</div>
					<div class="card-footer">
						<a href="#" class="card-link"><i class="fa fa-gittip "></i> Me
							Interesa </a> <a href="#" class="card-link"><i
							class="fa fa-comment "></i> Comenta </a> <a href="mensaje.jsp"
							class="card-link"><i class="fa fa-mail-forward "></i>
							Contacta </a>
				         <div>
							<textarea class="form-control" id="message" rows="3"
							placeholder="¿Qué estás pensando, <%=u.getNombreUsuario()%>?"
							name="descripcion"></textarea>
						</div>

							<div class="btn-toolbar justify-content-between">
								<div class="btn-group">
									<input type="submit" class="btn btn-primary" value="Comenta" />
								</div>
								<div class="btn-group"></div>
							</div>
					</div>

				</div>
				<%
					}
		} else response.sendRedirect("perfilAjeno.jsp?user="+request.getParameter("user"));
				%>
				<!-- Post /////-->

			</div>
			<div class="col-md-1"></div>
		</div>
		<!-- start footer Area -->
		<footer class="footer-area section-gap">
			<div class="container">
				<div class="row d-flex flex-column justify-content-center">
					<ul class="footer-menu">
						<%
						if(u.getRolUsuario().equals("admin")){
						%>
						<li><a href="muroAdmin.jsp#home">Inicio</a></li>
						<%
						} 
						%>
						<%
						if(u.getRolUsuario().equals("usuario")){
						%>
						<li><a href="muro.jsp#home">Inicio</a></li>
						<%
						} 
						%>
						<li><a href="pag/logout.jsp">Cerrar Sesión</a></li>
					</ul>
					<div class="footer-social">
						<a href="#"> <i class="fa fa-facebook"></i>
						</a> <a href="#"> <i class="fa fa-twitter"></i>
						</a> <a href="#"> <i class="fa fa-instagram"></i>
						</a>
					</div>
					<p class="footer-text m-0">
						<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
						Copyright &copy;
						<script>
							document.write(new Date().getFullYear());
						</script>
						| The team Pets&Friends TGN <i class="fa fa-heart-o"
							aria-hidden="true"></i>
						<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
					</p>
				</div>
			</div>
		</footer>
		<!-- End footer Area -->

		<script src="js/vendor/jquery-2.2.4.min.js"></script>
		<script
			src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"
			integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
			crossorigin="anonymous"></script>
		<script src="js/vendor/bootstrap.min.js"></script>
		<script src="js/jquery.ajaxchimp.min.js"></script>
		<script src="js/jquery.nice-select.min.js"></script>
		<script src="js/jquery.sticky.js"></script>
		<script src="js/parallax.min.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script src="js/jquery.magnific-popup.min.js"></script>
		<script src="js/main.js"></script>
</body>

</html>
