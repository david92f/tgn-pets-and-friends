<%@page import="utils.MascotaService"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.List"%>
<%@page import="modelos.Mascota"%>
<%@page import="modelos.Usuario"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>

<html lang="es">
<head>

<meta charset="ISO-8859-1">
<title>Mascotas</title>
<!-- Mobile Specific Meta -->
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Favicon-->
<link rel="shortcut icon" href="img/icono.ico">
<!-- Author Meta -->
<meta name="author" content="Colorlib">
<!-- Meta Description -->
<meta name="description" content="">
<!-- Meta Keyword -->
<meta name="keywords" content="">
<!-- Site Title -->
<title>Mascotas - TGN Pets &amp; Friends</title>

<link
	href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700"
	rel="stylesheet">

<link rel="stylesheet" href="css/linearicons.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/magnific-popup.css">
<link rel="stylesheet" href="css/nice-select.css">
<link rel="stylesheet"
	href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/main.css">
<link rel="stylesheet" href="css/muro.css">

<link
	href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
	rel="stylesheet"
	integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN"
	crossorigin="anonymous">

</head>


<body>

	<!-- Cargar los datos de la httpsession del usuario logeado -->
	<%
		Usuario u = (Usuario) session.getAttribute("usuario");
	%>

	<header class="default-header">
		<div class="container">
			<div class="header-wrap">
				<div
					class="header-top d-flex justify-content-between align-items-center">
					<div class="logo">
						<a href="muro.jsp#home"><img src="img/logo.png" alt=""></a>
					</div>

					<div class="main-menubar d-flex align-items-center">
						<nav class="hide">
							<a href="muro.jsp#home">Inicio</a> <a href="perfilPropio.jsp<%=u.getNomUsuario()%>">Perfil</a>
							<a href="pag/logout.jsp" class="btn btn-outline-secondary">Cerrar
								sesi�n </a>
						</nav>
						<div class="media">

							<div class="menu-bar">
								<span class="lnr lnr-menu"></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>



	<div id="home" class="container-fluid gedf-wrapper muro">
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<p>
				<h2>
					Mascotas de
					<%=u.getNombreUsuario()%>
					<%=u.getApellidoUsuario()%></h2>
				</p>
			</div>
			<div class="col-md-2"></div>
		</div>
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<%
					MascotaService ms = new MascotaService();
					Set<Mascota> lista = ms.getListMascotasById(u.getIdUsuario());
					for (Mascota m : lista) {
				%>
				<div class="card gedf-card">
					<div class="card-body">
						<div class="row">
							<div class="col-md-6">
								<img class="img-responsive" width="90%" src="<%=m.urlImagen() %>">
							</div>
							<div class="col-md-6">
								<h5 class="card-title text-center"><%=m.getNombreMascota()%></h5>
								<p class="card-text">
									<br>Datos de la mascota: <br>
									<%=m.getEspecieMascota()%><br>
									<%=m.getRazaMascota()%><br>
									<%=m.getSexoMascota()%><br>
									<%=m.getDescripcionMascota()%>
								</p>
							</div>
						</div>
					</div>
				</div>
				<%
					}
				%>

			</div>
			<div class="col-md-2"></div>
		</div>
	</div>
	<!-- start footer Area -->
	<footer class="footer-area section-gap">
		<div class="container">
			<div class="row d-flex flex-column justify-content-center">
				<ul class="footer-menu">
					<li><a href="muro.jsp#home">Inicio</a></li>
					<li><a href="pag/logout.jsp">Cerrar Sesi�n</a></li>
				</ul>
				<div class="footer-social">
					<a href="#"><i class="fa fa-facebook"></i></a> <a href="#"><i
						class="fa fa-twitter"></i></a> <a href="#"><i
						class="fa fa-instagram"></i></a>

				</div>
				<p class="footer-text m-0">
					<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
					Copyright &copy;
					<script>
						document.write(new Date().getFullYear());
					</script>
					| The team Pets&Friends <i class="fa fa-heart-o" aria-hidden="true"></i>
					<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
				</p>
			</div>

		</div>

	</footer>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
		integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
		integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
		crossorigin="anonymous"></script>


	<script src="js/vendor/jquery-2.2.4.min.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"
		integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
		crossorigin="anonymous"></script>
	<script src="js/vendor/bootstrap.min.js"></script>
	<script src="js/jquery.ajaxchimp.min.js"></script>
	<script src="js/jquery.nice-select.min.js"></script>
	<script src="js/jquery.sticky.js"></script>
	<script src="js/parallax.min.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/main.js"></script>

</body>
</html>