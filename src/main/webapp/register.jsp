<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html lang="es">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<!-- Mobile Specific Meta -->
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Favicon-->
<link rel="shortcut icon" href="img/icono.ico">
<!-- Author Meta -->
<meta name="author" content="Colorlib">
<!-- Meta Description -->
<meta name="description" content="">
<!-- Meta Keyword -->
<meta name="keywords" content="">

<!-- Título de la página actual -->
<title>Registro - TGN Pets &amp; Friends</title>

<!-- Estilos -->
<link
	href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700"
	rel="stylesheet">
<link rel="stylesheet" href="css/linearicons.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/magnific-popup.css">
<link rel="stylesheet" href="css/nice-select.css">
<link rel="stylesheet"
	href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/main.css">
<link rel="stylesheet" href="css/nuestrosEstilos.css">

<!--  <link href="css/style.css" rel="stylesheet" type="text/css" /> estilo para formulario anterior-->

</head>

<body>
	<!-- Barra de navegación -->
	<header class="default-header">
		<div class="container">
			<div class="header-wrap">
				<div
					class="header-top d-flex justify-content-between align-items-center">
					<div class="logo">
						<a href="index.jsp"><img src="img/logo.png" alt=""></a>
					</div>
					<div class="main-menubar d-flex align-items-center">
						<nav class="hide">
							<a href="index.jsp#home">Inicio</a> <a href="index.jsp#project">Proyecto</a>
							<a href="index.jsp#equipo">Equipo</a> <a href="index.jsp#donate">Donar</a>
							<a href="index.jsp#about" class="btn btn-outline-secondary">Iniciar
								Sesión</a>
						</nav>
						<div class="menu-bar">
							<span class="lnr lnr-menu"></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>

	<!-- Formulario de registro -->
	<div class="container-fluid formRegistro">
		<div class="row">
			<div class="col-2"></div>
			<div class="col-8">
				<div class="row">
					<div class="col-sm-12">
						<h2 class="titulo2">Introduce tus datos:</h2>
					</div>
				</div>
				<form action="RegisterServlet" method="post">
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Usuario </label>
						<div class="col-sm-2">
							<input class="form-check-input" type="radio" name="rol" id="rol1"
								value="usuario" checked> <label class="form-check-label"
								for="rol1">Particular</label>
						</div>
						<div class="col-sm-2">
							<input class="form-check-input" type="radio" name="rol" id="rol2"
								value="protectora"> <label class="form-check-label"
								for="rol2">Protectora</label>
						</div>
						<label for="fechaNac" class="col-sm-2 col-form-label">Fecha
							de nacimiento</label>
						<div class="col-sm-4">
							<input type="date" class="form-control" name="fechaNac"
								id="fechaNac">
						</div>
					</div>
					<div class="form-group row">
						<label for="nomUsuario" class="col-sm-2 col-form-label">Nombre
						</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" name="nomUsuario"
								id="nomUsuario" placeholder="Nombre" required="required">
						</div>
						<label for="tlfUsuario" class="col-sm-2 col-form-label">Nombre
							de usuario</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" name="alias" id="alias"
								placeholder="Nombre de usuario" required="required">
						</div>
					</div>
					<div class="form-group row">
						<label for="apelUsuario" class="col-sm-2 col-form-label">Apellidos
						</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" name="apelUsuario"
								id="apelUsuario" placeholder="Apellidos">
						</div>
						<label for="contraUsuario" class="col-sm-2 col-form-label">Contraseña</label>
						<div class="col-sm-4">
							<input type="password" class="form-control" name="contra1"
								id="contraUsuario" placeholder="Contraseña" required="required">
						</div>
					</div>
					<div class="form-group row">
						<label for="emailUsuario" class="col-sm-2 col-form-label">Email</label>
						<div class="col-sm-4">
							<input type="email" class="form-control" name="emailUsuario"
								id="emailUsuario" placeholder="Email" required="required">
						</div>
						<label for="contraUsuario" class="col-sm-2 col-form-label">Confirma
							contraseña</label>
						<div class="col-sm-4">
							<input type="password" class="form-control" name="contra2"
								id="contraUsuario" placeholder="Confirma contraseña"
								required="required">
						</div>
					</div>
					<div class="form-group row">
						<label for="telefonoUsuario" class="col-sm-2 col-form-label">Teléfono</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" name="telefonoUsuario"
								id="telefonoUsuario" placeholder="Télefono" required="required">
						</div>
						<div class="col-sm-3">
							<input type="submit" class="btn btn-success btn-lg btn-block"
								value="Registrarme" />
						</div>
						<div class="col-sm-3">
							<a href="index.jsp" class="btn btn-danger btn-lg btn-block"
								role="button">Cancelar</a>
						</div>
					</div>
				</form>
			</div>
			<div class="col-2"></div>
		</div>
	</div>

	<!-- Formulario de registro anterior 
	<h3>Student Registration Form</h3>
	<form action="RegisterServlet" method="POST">

		<table align="center" cellpadding="10">
			<tr>
				<td>First Name</td>
				<td><input type="text" name="firstName" maxlength="30" /> (max
					30 characters a-z and A-Z)</td>
			</tr>
			<tr>
				<td>Middle Name</td>
				<td><input type="text" name="middleName" maxlength="30" />
					(max 30 characters a-z and A-Z)</td>
			</tr>

			<tr>
				<td>Last Name</td>
				<td><input type="text" name="lastName" maxlength="30" /> (max
					30 characters a-z and A-Z)</td>
			</tr>

			<tr>
				<td>Email</td>
				<td><input type="text" name="email" maxlength="100" /></td>
			</tr>

			<tr>
				<td>User ID</td>
				<td><input type="text" name="userId" maxlength="100" /></td>
			</tr>
			<tr>
				<td>Password</td>
				<td><input type="text" name="password" maxlength="100" /></td>
			</tr>

			<tr>
				<td colspan="2" align="center"><input type="submit"
					value="Submit"> <input type="reset" value="Reset">
				</td>
			</tr>
		</table>

	</form>
-->

	<!-- start footer Area -->
	<footer class="footer-area section-gap">
		<div class="container">
			<div class="row d-flex flex-column justify-content-center">
				<ul class="footer-menu">
					<li><a href="index.jsp#home">Inicio</a></li>
					<li><a href="index.jsp#project">Proyecto</a></li>
					<li><a href="index.jsp#equipo">Equipo</a></li>
					<li><a href="index.jsp#donate">Donar</a></li>
					<li><a href="index.jsp#about">Inicio Sesión</a></li>
				</ul>
				<div class="footer-social">
					<a href="#"><i class="fa fa-facebook"></i></a> <a href="#"><i
						class="fa fa-twitter"></i></a> <a href="#"><i
						class="fa fa-instagram"></i></a>

				</div>
				<p class="footer-text m-0">
					<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
					Copyright &copy;
					<script>
						document.write(new Date().getFullYear());
					</script>
					| The team Pets&Friends <i class="fa fa-heart-o" aria-hidden="true"></i>
					<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
				</p>
			</div>
		</div>
	</footer>
	<!-- Scripts -->
	<script src="js/vendor/jquery-2.2.4.min.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"
		integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
		crossorigin="anonymous"></script>
	<script src="js/vendor/bootstrap.min.js"></script>
	<script src="js/jquery.ajaxchimp.min.js"></script>
	<script src="js/jquery.nice-select.min.js"></script>
	<script src="js/jquery.sticky.js"></script>
	<script src="js/parallax.min.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/main.js"></script>

</body>
</html>