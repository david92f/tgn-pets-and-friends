<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="es">
<head>

<!-- Para añadir un icono a la ventana  -->
<link rel="shortcut icon" href="img/icono.ico">
	<!-- Título de la página actual -->
<title>Inicio - TGN Pets &amp; Friends</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<!-- Mobile Specific Meta -->
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Author Meta -->
<meta name="author" content="Colorlib">
<!-- Meta Description -->
<meta name="description" content="">
<!-- Meta Keyword -->
<meta name="keywords" content="">

<link
href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700"
rel="stylesheet">
<link rel="stylesheet" href="css/linearicons.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/magnific-popup.css">
<link rel="stylesheet" href="css/nice-select.css">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/main.css">
</head>

<body>
	<!-- Barra de navegación -->
	<header class="default-header">
	<div class="container">
		<div class="header-wrap">
			<div
				class="header-top d-flex justify-content-between align-items-center">
				<div class="logo">
					<a href="#home"><img src="img/logo.png" alt=""></a>
				</div>
				<div class="main-menubar d-flex align-items-center">
					<nav class="hide"> <a href="#home">Inicio</a> <a
						href="#project">Proyecto</a> <a href="#equipo">Equipo</a> <a
						href="#donate">Donar</a> <a href="#about"
						class="btn btn-outline-secondary">Iniciar Sesión</a> </nav>
					<div class="menu-bar">
						<span class="lnr lnr-menu"></span>
					</div>
				</div>
			</div>
		</div>
	</div>
	</header>
	<!-- Banner inicio -->
	<section class="banner-area relative" id="home">
	<div class="overlay overlay-bg"></div>
	<div class="container">
		<div class="row fullscreen align-items-center justify-content-start"
			style="height: 915px;">
			<div class="banner-content col-lg-9 col-md-12">
				<h1>Never Alone!</h1>
				<a href="#donate" class="head-btn btn text-uppercase">Donar</a>
			</div>
		</div>
	</div>
	</section>
	<!-- Información sobre el proyecto -->
	<section class="project-area section-gap" id="project">
	<div class="container">
		<div class="row d-flex justify-content-center">
			<div class="col-md-8 pb-80 header-text">
				<h1>El Proyecto</h1>
				<p>
					Nuestro objetivo es evitar que nuestras mascotas se sientan solas y
					prevenir el abandono. Hemos desarrollado una plataforma
						para prevenir estos casos. 
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4 col-md-4 project-wrap">
				<div class="single-project">


					<div class="content-overlay"></div>
					<img class="content-image img-fluid d-block mx-auto"
						src="img/p1.jpg">
				</div>
				<div class="details">
					<center>
						<h2>Cuida</h2>
					</center>
					<p>Puedes encontrar a cuidadores como amante de los animales,
						por su estilo de vida, no pueden tener uno, pero les encantaria
						pasar tiempo con ellos y estan dispuestos a cuidarlos.</p>
				</div>

			</div>
			<div class="col-lg-4 col-md-4 project-wrap">
				<div class="single-project">

					<div class="content-overlay"></div>
					<img class="content-image img-fluid d-block mx-auto"
						src="img/p2.jpg">
				</div>
				<div class="details">
					<center>
						<h2>Adopta</h2>
					</center>
					<p>Si estas buscando adoptar a una mascota, tenemos usuarios y
						protectoras que tienen a tu mascota ideal.</p>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 project-wrap">
				<div class="single-project">

					<div class="content-overlay"></div>
					<img class="content-image img-fluid d-block mx-auto"
						src="img/p3.jpg">
				</div>
				<div class="details">
					<center>
						<h2>Ofrece</h2>
					</center>
					<p>Ofrece recursos a las protectoras para que los animales
						puedan tener un nivel de vida adecuado, con espacios libres,
						alimentación sana y evitar el sacrificio por falta de espacio en
						estos lugares.</p>
				</div>
			</div>

		</div>
	</div>
	</section>

	<!-- Formulario para inicar sesión -->
	<section id="about" class="project-area section-gap">

	<div class="wrapper muro">

		<div id="formContent">

			<div class="fadeIn first">
				<h3>Iniciar Sesión</h3>
				<p></p>

			</div>

			<form method="post" action="LoginServlet">
				<input name="userId" type="text" class="form-login" title="Username"
					value="" placeholder="Nombre de usuario" /> <input name="password"
					type="password" class="form-login" title="Password" value=""
					placeholder="Contraseña" />
				<p></p>
				<input type="submit" value="Entrar" class="btn btn-primary btn-sm" />
				<a href="register.jsp" class="btn btn-secondary btn-sm"
					role="button">Registrarme</a>

			</form>
		</div>

	</div>
	</section>

	<!-- Info sobre el equipo -->
	<section class="volunteer-area section-gap" id="equipo">
	<div class="container">
		<div class="row d-flex justify-content-center">
			<div class="col-md-8 pb-80 header-text">
				<h1>El Equipo</h1>
				<p>
					El equipo está formado por 4 jóvenes amantes de los animales y su
					propósito es prevenir el abadono y tener otra alternativa
						más para que nuestras mascotas estén en compañía. 
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-3 col-md-3 vol-wrap">
				<div class="single-vol">
					<div class="content">
						<a target="_blank"> <img
							class="content-image img-fluid d-block mx-auto" src="img/v1.jpg"
							alt="">
								<p></p>
								<center>
									<h4>Daniel Mimbrero</h4>
								</center>
								<p>
									Front-end e imagen corporativa<br><strong>Cerebro</strong>
								</p></a>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 vol-wrap">
				<div class="single-vol">
					<div class="content">
						<a target="_blank"> <img
							class="content-image img-fluid d-block mx-auto" src="img/v2.jpg"
							alt="">
								<p></p>
								<center>
									<h4>David Fernández</h4>
								</center>
								<p>
									Back-end y base de datos<br><strong>Monitor-Evaluador</strong>
								</p></a>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 vol-wrap">
				<div class="single-vol">
					<div class="content">
						<a target="_blank"> <img
							class="content-image img-fluid d-block mx-auto" src="img/v3.jpg"
							alt="">
								<p></p>
								<center>
									<h4>Camila Figueroa</h4>
								</center>
								<p>
									Base de datos y Front-end<br><strong>Finalizadora</strong>
								</p></a>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 vol-wrap">
				<div class="single-vol">
					<div class="content">
						<a target="_blank"> <img
							class="content-image img-fluid d-block mx-auto" src="img/v4.jpg"
							alt="">
								<p></p>
								<center>
									<h4>Felipe Zapata</h4>
								</center>
								<p>
									Back-end<br><strong>Cohesionador</strong>
								</p></a>
					</div>
				</div>
			</div>

		</div>
	</div>


	<!-- Start donate Area --> <section
		class="donate-area relative section-gap" id="donate">
	<div class="overlay overlay-bg"></div>
	<div class="container">
		<div class="row d-flex justify-content-center">
			<div class="col-lg-6 col-sm-12 pb-80 header-text">
				<h1 align="center">Donar</h1>
				<p>Ofrece recursos a las protectoras para que los animales
					puedan tener un nivel de vida adecuado, con espacios libres,
					alimentación sana y evitar el sacrificio por falta de espacio en
					estos lugares.</p>
			</div>
		</div>
		<div class="row d-flex justify-content-center">

			<div class="col-lg-6 contact-right">
				<form class="booking-form" id="myForm" action="donate.php">
					<div class="row">
						<div class="col-lg-12 d-flex flex-column">
							<select name="type" class="app-select form-control" required>
								<option data-display="Que desea donar?"></option>
								<option value="1">Alimentos</option>
								<option value="2">Medicamentos</option>
								<option value="3">Fondos</option>
							</select>
						</div>
						<div class="col-lg-6 d-flex flex-column">
							<input name="fname" placeholder="Nombre completo"
								onfocus="this.placeholder = ''"
								onblur="this.placeholder = 'Introduce tu nombre'"
								class="form-control mt-20" required="" type="text" required>
						</div>
						<div class="col-lg-6 d-flex flex-column">
							<input name="email" placeholder="Correo"
								pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$"
								onfocus="this.placeholder = ''"
								onblur="this.placeholder = 'Pon tu e-mail'"
								class="form-control mt-20" required="" type="email">
						</div>
						<div class="col-lg-12 d-flex flex-column">
							<input name="amound"
								placeholder="Introduce cantidad solo en caso de Fondos"
								onfocus="this.placeholder = ''"
								onblur="this.placeholder = 'Selecciona la cantidad solo en caso de Fondos'"
								class="form-control mt-20" required="" type="text"> <textarea
									class="form-control mt-20" name="message" placeholder="Mensaje"
									onfocus="this.placeholder = ''"
									onblur="this.placeholder = 'Mensaje'" required=""></textarea>
						</div>

						<div class="col-lg-12 d-flex justify-content-end send-btn">
							<button class="submit-btn primary-btn mt-20 text-uppercase ">
								Donar<span class="lnr lnr-arrow-right"></span>
							</button>
						</div>

						<div class="alert-msg"></div>
					</div>
				</form>
				<p class="payment-method">
					<img src="img/payment.png" alt="">
				</p>
			</div>
		</div>
	</div>
	</section> 
	<!-- End donate Area --> 
	
	<!-- start footer Area --> 
	<footer class="footer-area section-gap">
	<div class="container">
		<div class="row d-flex flex-column justify-content-center">
			<ul class="footer-menu">
			
				<li><a href="#home">Inicio</a></li>
				<li><a href="#project">Proyecto</a></li>
				<li><a href="#equipo">Equipo</a></li>
				<li><a href="#donate">Donar</a></li>
				<li><a href="#about">Iniciar Sesión</a></li>
				
			</ul>
			<div class="footer-social">
				<a href="#"><i class="fa fa-facebook"></i></a> <a href="#"><i
					class="fa fa-twitter"></i></a> <a href="#"><i
					class="fa fa-instagram"></i></a>

			</div>
			<p class="footer-text m-0">
				<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
				Copyright &copy;
				<script>
					document.write(new Date().getFullYear());
				</script>
				| The team Pets&Friends <i class="fa fa-heart-o" aria-hidden="true"></i>
				<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
			</p>
		</div>
	</div>
	</footer> 
	
	<!-- Scripts --> 
	<script src="js/vendor/jquery-2.2.4.min.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"
		integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
		crossorigin="anonymous"></script> <script
		src="js/vendor/bootstrap.min.js"></script> <script
		src="js/jquery.ajaxchimp.min.js"></script> <script
		src="js/jquery.nice-select.min.js"></script> <script
		src="js/jquery.sticky.js"></script> <script src="js/parallax.min.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script src="js/jquery.magnific-popup.min.js"></script> <script
		src="js/main.js"></script>
</body>
</html>
