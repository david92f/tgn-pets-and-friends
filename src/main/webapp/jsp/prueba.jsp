<%@page import="java.util.Base64"%>
<%@page import="org.hibernate.Session"%>
<%@page contentType="text/html; charset=utf-8"
	import="utils.HibernateUtils,modelos.Usuario"%>
<html>
<head>
<title>Prueba</title>
</head>
<body>
	<%
		Session s = new HibernateUtils().getSessionFactory();
		s.beginTransaction();
		Usuario u = (Usuario) s.get(Usuario.class, 3);
		s.close();
	%>
	<h1>Datos del usuario:</h1>
	<p>ID: <%=u.getIdUsuario()%></p>
	<p><%=u.getNombreUsuario()%></p>
	<p><%=u.getApellidoUsuario()%></p>
	<p><%=u.getEmailUsuario()%></p>
	<p><%=u.getFechaNacimiento()%></p>
	<p><%=u.getTelefonoUsuario()%></p>
	<p><%=u.getNomUsuario()%></p>
	<p><%=u.getClaveUsuario()%></p>
	<img src="<%=u.urlImagen()%>" />
</body>
</html>