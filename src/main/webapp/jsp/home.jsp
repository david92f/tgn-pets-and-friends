<%@page import="utils.LoginService"%>
<%@page import="modelos.Usuario"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Date"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	 <link rel="stylesheet" type="text/css" href="css/style.css"/>
	 <title>Result Page</title>	
</head>
<body>

	 <div id="container">
		 <h1>Result Page</h1>
			 <b>This is Sample Result Page</b><br/>
			 <%=new Date()%><br/>
			 <%
				 Usuario usuario = (Usuario) session.getAttribute("usuario");
			 %>		
			 <b>Welcome <%= usuario.getNombreUsuario() + " " + usuario.getApellidoUsuario()%></b>		
			 <br/>
			 <p><a href="logout.jsp">Logout</a></p>

		 <table>
			 <thead>
				 <tr>
					 <th>usuario ID</th>
					 <th>First Name</th>
					 <th>Last Name</th>
					 <th>email</th>					
				 </tr>
			 </thead>
			 <tbody>
				 <%
					 LoginService loginService = new LoginService();
					 List<Usuario> list = loginService.getListOfUsers();
					 for (Usuario u : list) {
				 %>
				 <tr>
					 <td><%=u.getIdUsuario()%></td>
					 <td><%=u.getNombreUsuario()%></td>
					 <td><%=u.getApellidoUsuario()%></td>
					 <td><%=u.getEmailUsuario()%></td>
				 </tr>
				 <%}%>
			 <tbody>
		 </table>		
		 <br/>
	 </div>
	
</body>
</html>
